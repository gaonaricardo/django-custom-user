from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser

# Register your models here.

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('email', 'is_staff', 'is_active',)
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields':('password', 'email', 'username', 'phone_number')}),
        ('Permissions', {'fields': ('user_permissions', 'groups', 'is_staff', 'is_active',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2', 'phone_number', 'is_staff', 'is_active')
        }),
    )
    search_fields=('email',)
    ordering = ('email', )
    filter_horizontal=('groups', 'user_permissions')

admin.site.register(CustomUser, CustomUserAdmin)

